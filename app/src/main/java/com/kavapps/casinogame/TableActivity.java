package com.kavapps.casinogame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class TableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}