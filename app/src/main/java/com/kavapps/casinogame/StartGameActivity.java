package com.kavapps.casinogame;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.functions.Action;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class StartGameActivity extends AppCompatActivity {

    @BindView(R.id.layButtonGame)
    LinearLayout layButtonGame;

    @BindView(R.id.layLoadingGame)
    LinearLayout layLoadingGame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_game_activity);
        ButterKnife.bind(this);

        loadGame();
    }

    private void loadGame() {
        Completable.timer(4, TimeUnit.SECONDS,
                AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    layLoadingGame.setVisibility(View.INVISIBLE);
                    layButtonGame.setVisibility(View.VISIBLE);
                });

    }

    @OnClick(R.id.buttonExit)
    void onClickExit(){
        finish();
    }

    @OnClick(R.id.buttonStart)
    void onClickStart(){
        Intent startGameIntent = new Intent(this,GameActivity.class);
        startActivity(startGameIntent);
        finish();
    }
}