package com.kavapps.casinogame;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GameActivity extends AppCompatActivity {


    @BindView(R.id.image1)
    ImageView image1;

    @BindView(R.id.image2)
    ImageView image2;

    @BindView(R.id.image3)
    ImageView image3;

    @BindView(R.id.textBet)
    TextView textBet;

    @BindView(R.id.textCredit)
    TextView textCredit;

    @BindView(R.id.textWin)
    TextView textWin;

    @BindView(R.id.btnPayTable)
    ImageView btnPayTable;

    @BindView(R.id.btnBetOne)
    ImageView btnBetOne;

    @BindView(R.id.btnBetMax)
    ImageView btnBetMax;

    @BindView(R.id.btnSpin)
    ImageView btnSpin;

    private List<Item> itemList = new ArrayList<>();
    private int credits = 500;
    private int winCredits = 0;
    private int bet = 10;
    private boolean mStopHandler = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        textWin.setText(String.valueOf(winCredits));
        textCredit.setText(String.valueOf(credits));
        textBet.setText(String.valueOf(bet));
        initListData();
        setImageToItem();
    }


    private void setImageToItem() {
        int randomForItem1 = new Random().nextInt(7);
        int randomForItem2 = new Random().nextInt(7);
        int randomForItem3 = new Random().nextInt(7);
        image1.setImageDrawable(getDrawable(itemList.get(randomForItem1).getId()));
        image2.setImageDrawable(getDrawable(itemList.get(randomForItem2).getId()));
        image3.setImageDrawable(getDrawable(itemList.get(randomForItem3).getId()));
    }

    private void checkResult(int a, int b, int c) {
        if (a == b && a == c) {
            switch (a) {
                case 0: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_0;
                        winCredits += WinCredits.BET_10_ITEM_0;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_0;
                        winCredits += WinCredits.BET_20_ITEM_0;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_0;
                        winCredits += WinCredits.BET_30_ITEM_0;
                    }
                    break;
                }

                case 1: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_1;
                        winCredits += WinCredits.BET_10_ITEM_1;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_1;
                        winCredits += WinCredits.BET_20_ITEM_1;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_1;
                        winCredits += WinCredits.BET_30_ITEM_1;
                    }
                    break;
                }

                case 2: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_2;
                        winCredits += WinCredits.BET_10_ITEM_2;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_2;
                        winCredits += WinCredits.BET_20_ITEM_2;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_2;
                        winCredits += WinCredits.BET_30_ITEM_2;
                    }
                    break;
                }

                case 3: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_3;
                        winCredits += WinCredits.BET_10_ITEM_3;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_3;
                        winCredits += WinCredits.BET_20_ITEM_3;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_3;
                        winCredits += WinCredits.BET_30_ITEM_3;
                    }
                    break;
                }

                case 4: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_4;
                        winCredits += WinCredits.BET_10_ITEM_4;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_4;
                        winCredits += WinCredits.BET_20_ITEM_4;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_4;
                        winCredits += WinCredits.BET_30_ITEM_4;
                    }
                    break;
                }

                case 5: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_5;
                        winCredits += WinCredits.BET_10_ITEM_5;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_5;
                        winCredits += WinCredits.BET_20_ITEM_5;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_5;
                        winCredits += WinCredits.BET_30_ITEM_5;
                    }
                    break;
                }

                case 6: {
                    if (bet == 10) {
                        credits += WinCredits.BET_10_ITEM_6;
                        winCredits += WinCredits.BET_10_ITEM_6;
                    } else if (bet == 20) {
                        credits += WinCredits.BET_20_ITEM_6;
                        winCredits += WinCredits.BET_20_ITEM_6;
                    } else if (bet == 30) {
                        credits += WinCredits.BET_30_ITEM_6;
                        winCredits += WinCredits.BET_30_ITEM_6;
                    }
                    break;
                }
            }
            textCredit.setText(String.valueOf(credits));
            textWin.setText(String.valueOf(winCredits));
        }
    }

    private void initListData() {
        itemList.add(new Item(R.drawable.item0));
        itemList.add(new Item(R.drawable.item1));
        itemList.add(new Item(R.drawable.item2));
        itemList.add(new Item(R.drawable.item3));
        itemList.add(new Item(R.drawable.item4));
        itemList.add(new Item(R.drawable.item5));
        itemList.add(new Item(R.drawable.item6));
    }

    @OnClick(R.id.btnPayTable)
    void onClickButtonPayTable() {
        Intent startTableActivityIntent = new Intent(this, TableActivity.class);
        startActivity(startTableActivityIntent);
    }

    @OnClick(R.id.btnSpin)
    void onClickButtonSpin() {
        if (bet > credits) {
            Toast.makeText(this, "Not enough credits", Toast.LENGTH_SHORT).show();
            return;
        }
        credits = credits - bet;
        textCredit.setText(String.valueOf(credits));
        twisting();
    }

    @OnClick(R.id.btnBetMax)
    void onClickButtonBetMax() {
        bet = 30;
        textBet.setText(String.valueOf(bet));
    }

    @OnClick(R.id.btnBetOne)
    void onClickButtonBetOne() {
        if (bet == 10) {
            bet = 20;
            textBet.setText(String.valueOf(bet));
        } else if (bet == 20) {
            bet = 30;
            textBet.setText(String.valueOf(bet));
        } else if (bet == 30) {
            bet = 10;
            textBet.setText(String.valueOf(bet));
        }
    }

    private void twisting() {
        blockButtons();
        Handler handler = new Handler();
        final int[] time = {15};
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                int randomForItem1 = new Random().nextInt(7);
                int randomForItem2 = new Random().nextInt(7);
                int randomForItem3 = new Random().nextInt(7);
                image1.setImageDrawable(getDrawable(itemList.get(randomForItem1).getId()));
                image2.setImageDrawable(getDrawable(itemList.get(randomForItem2).getId()));
                image3.setImageDrawable(getDrawable(itemList.get(randomForItem3).getId()));
                time[0]--;
                if (time[0] == 0) {
                    mStopHandler = true;
                }

                if (!mStopHandler) {
                    handler.postDelayed(this, 100);
                } else {
                    checkResult(randomForItem1, randomForItem2, randomForItem3);
                    unBlockButtons();
                }
            }
        };
        if (mStopHandler) {
            handler.removeCallbacks(runnable);
            mStopHandler = false;
        }
        handler.post(runnable);
    }

    private void blockButtons(){
        btnPayTable.setClickable(false);
        btnBetOne.setClickable(false);
        btnBetMax.setClickable(false);
        btnSpin.setClickable(false);
        btnPayTable.setImageDrawable(getDrawable(R.drawable.button_pay_table_selector_clicked));
        btnBetOne.setImageDrawable(getDrawable(R.drawable.button_bet_one_selector_clicked));
        btnBetMax.setImageDrawable(getDrawable(R.drawable.button_bet_max_selector_clicked));
        btnSpin.setImageDrawable(getDrawable(R.drawable.button_spin_selector_clicked));
    }

    private void unBlockButtons(){

        btnPayTable.setClickable(true);
        btnBetOne.setClickable(true);
        btnBetMax.setClickable(true);
        btnSpin.setClickable(true);


        btnSpin.setImageDrawable(getDrawable(R.drawable.button_spin_selector));
        btnPayTable.setImageDrawable(getDrawable(R.drawable.button_pay_table_selector));
        btnBetOne.setImageDrawable(getDrawable(R.drawable.button_bet_one_selector));
        btnBetMax.setImageDrawable(getDrawable(R.drawable.button_bet_max_selector));

    }


}