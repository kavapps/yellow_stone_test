package com.kavapps.casinogame;

class WinCredits {

    public static final int BET_10_ITEM_0 = 2500;
    public static final int BET_10_ITEM_1 = 1000;
    public static final int BET_10_ITEM_2 = 500;
    public static final int BET_10_ITEM_3 = 200;
    public static final int BET_10_ITEM_4 = 100;
    public static final int BET_10_ITEM_5 = 50;
    public static final int BET_10_ITEM_6 = 20;

    public static final int BET_20_ITEM_0 = 5000;
    public static final int BET_20_ITEM_1 = 2000;
    public static final int BET_20_ITEM_2 = 1000;
    public static final int BET_20_ITEM_3 = 400;
    public static final int BET_20_ITEM_4 = 200;
    public static final int BET_20_ITEM_5 = 100;
    public static final int BET_20_ITEM_6 = 40;

    public static final int BET_30_ITEM_0 = 7500;
    public static final int BET_30_ITEM_1 = 3000;
    public static final int BET_30_ITEM_2 = 1500;
    public static final int BET_30_ITEM_3 = 600;
    public static final int BET_30_ITEM_4 = 500;
    public static final int BET_30_ITEM_5 = 150;
    public static final int BET_30_ITEM_6 = 60;


}
